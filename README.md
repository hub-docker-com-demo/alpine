# [alpine](https://hub.docker.com/_/alpine/)

A minimal Docker image based on Alpine Linux with a complete package index and only 5 MB in size!

(Unofficial demo and howto)


* [*Tutorials and Howtos*](https://wiki.alpinelinux.org/wiki/Tutorials_and_Howtos)

* [*apk fails to update/add*](https://github.com/gliderlabs/docker-alpine/issues/55) (`apk` config)

* [*How to get regular stuff working*](https://wiki.alpinelinux.org/wiki/How_to_get_regular_stuff_working)

* [*Alpine Linux Init System*](https://wiki.alpinelinux.org/wiki/Alpine_Linux_Init_System)

* [Almquist shell](https://en.wikipedia.org/wiki/Almquist_shell)

* [coreutils](https://pkgs.alpinelinux.org/packages?name=coreutils)@pkgs.alpinelinux

* See also Wolfi https://gitlab.com/hub-docker-com-demo/wolfi

# Demo contents
* Using "entrypoint" to change default shell.
* Adding a user and executing the shell as non-root.
* Allowing the non-root user to use sudo.
  * Using some info from https://gitlab.com/alpinelinux-packages-demo/sudo
* Installing (apk) packages before executing the (yaml) script.
* Timing packages installation and yaml script execution time with "time".
* See [.gitlab-ci.yml](https://gitlab.com/hub-docker-com-demo/alpine/blob/master/.gitlab-ci.yml) file.

# Shell known to be able to replace Busybox (a)sh
* [bash](https://gitlab.com/alpinelinux-packages-demo/bash)

# Other demo related to Unix users
* [node](https://gitlab.com/hub-docker-com-demo/node)

# Alpine

|                       |                                                      |
|                   ---:|---                                                   |
| kernel                | linux                                                |
| libc                  | [musl](#musl-libc)                                   |
| compiler              | gcc?                                                 |
| init                  | [OpenRC](https://wiki.alpinelinux.org/wiki/Alpine_Linux_Init_System)  |
| shell                 | Busybox (a)sh                                          |
| Unix-like basic tools | Busybox                                              |
| man                   | [mandoc](#mandoc)                                    |
| man l10n              | Seems not to work with lxc-doc (Alpine 3.7)          |

## OpenRC
* [hub-docker-com-demo/nginx](https://gitlab.com/hub-docker-com-demo/nginx)

# [Alpine Packages Demo](https://gitlab.com/alpinelinux-packages-demo)
* Includes man pages

# musl libc
* [Environment Variables](https://wiki.musl-libc.org/environment-variables.html)
* [Functional differences from glibc](https://wiki.musl-libc.org/functional-differences-from-glibc.html)
* [Musl](https://en.wikipedia.org/wiki/Musl)

# Mandoc
* [mandoc](http://mandoc.bsd.lv/) [WikiPedia](https://en.wikipedia.org/wiki/Mandoc)
* [man](http://mandoc.bsd.lv/man/man.1.html) (man)
* [mandoc](http://mandoc.bsd.lv/man/mandoc.1.html) (man)
* [man](https://pkgs.alpinelinux.org/packages?name=man) (Alpine package)
* [*Writing Effective Manual Pages*](http://home.windstream.net/kollar/groff/effman.html) Larry Kollar 2004 (nroff, groff commands)

# Python docker images
* [*Using Alpine can make Python Docker builds 50× slower*
  ](https://pythonspeed.com/articles/alpine-docker-python/)
  2020-01..2021-05 Itamar Turner-Trauring

# LXC
* See [gitlab.com/alpinelinux-packages-demo/lxc](https://gitlab.com/alpinelinux-packages-demo/lxc)
